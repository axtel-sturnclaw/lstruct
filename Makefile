# Build Script for lStruct
CC = gcc

CFLAGS += -Wall -Wextra -fPIC -Iinc/
Q = @

ODIR = out
SRC := src/lstruct.c
OBJ := $(SRC:src/%.c=$(ODIR)/%.o)

OUT := lstruct.so

LUAVER = 5.3
LUALIBDIR = /usr/local/lib/lua/$(LUAVER)

.PHONY: default release debug clean install link uninstall

default: release

release: CFLAGS += -O2 -Werror
debug: CFLAGS += -g -O1
debug: Q =

release: $(OUT)
debug: $(OUT)

out/%.o: src/%.c inc/lstruct.h $(ODIR)
	@echo Compiling $<
	$(Q) $(CC) -c -o $@ $< $(CFLAGS)

$(OUT): $(OBJ)
	@echo "Linking ( $(OBJ) ) as $@."
	$(Q) $(CC) -o $@ -shared $^ $(CFLAGS) $(LDFLAGS)

clean:
	@rm -r $(ODIR) $(OUT)

$(ODIR):
	@mkdir -p $@

link: $(OUT)
	@echo Linking $^ as $(LUALIBDIR)/$^
	@ln -sf $(CURDIR)/$^ $(LUALIBDIR)/$^

install: $(OUT)
	@echo Installing $^ as $(LUALIBDIR)/$^
	@install $^ $(LUALIBDIR)/

uninstall:
	@echo Uninstalling $(LUALIBDIR)/$^
	@rm $(LUALIBDIR)/$(OUT)
