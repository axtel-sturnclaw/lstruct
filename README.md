## About

Structs are an implementation of a robust and lightweight data storage type for
Lua.

Designed for maximum power, structs feature a simple base-instance inheritance
model, optional getters and setters, and full meta-method support.  A C
implementation is also provided for use by other modules.

## Example

See `example.lua` for an in-depth example implementation of a vector data-type
with this module.

```lua

local struct = require 'struct'
local test = struct 'test'

function test:__init()
	test._test = "this is a "
end

function test:__get_test() return self._test .. "test" end

local i = test()

assert(i.test == "this is a test")

```

## Usage

lStruct provides one anonymous function, referred to in this document as
`struct`. This function is invoked as `struct (name)`, where `name` is a string
or nil.

This creates and returns an optionally named, empty struct.  For the purposes of
this document, we will call it the highly original name of `nstruct`.  You can
add functions to this struct, or instantiate it at any time.

Instantiation of the struct is simple with a call to `nstruct(...)`, where `...`
is any number of arguments to be passed to the struct's initializer function.

By default the initializer function is named `__init`, and is called with the
new struct as the first argument, and any arguments provided to `nstruct(...)`.

The initializer function needs not exist to instantiate a struct, and does not
perform any required setup of the struct instance itself; it is passed a fully
created struct instance and should not attempt to modify the instance's
metatable.

Structs have support for precise getters and setters for values associated with
string keys, and less precise getters/setters for other key types and string
keys without a specific getter/setter.

## The API

Structs are created using an anonymous function `struct(name)`, returned by
`require 'struct'`; it is called with an optional name for the generated struct,
and returns a new, empty struct.

Structs have two fields that must not be altered; `__index` drives getters for a
struct, and `__newindex` drives setters for a struct.

Structs are provided with a courtesy `__tostring` implementation, though it may
be replaced at your discretion.

A function named `__init` is called on struct instantiation, and is passed the
struct instance and any arguments provided to the instantiation call.

All meta-methods used by Lua are supported, with the exeception of `__index` and
`__newindex`; these are replaced by finer-grain getters and setters.

Functions in a struct respectively prefixed with `__get_` or `__set_` are
treated as getters and setters for string keys not prefixed with `_`.

If no getter or setter is applicable or found, functions respectively prefixed
`__index_by_` and `__setindex_by_` are used as type-based getters and setters.

If no getter is found, a regular lookup is performed in the struct the instance
inherits from; if no setter is found a `rawset()` is performed on the struct
instance.

The format for string-key getters is `function nstruct:__get_test()`, and for
setters is `function nstruct:__set_test(val)`, where `test` is the string key in
question; these are called on `val = inst.test` and `inst.test = val`,
respectively.

The format for typed getters is `function __index_by_type(key)`, where type is
one of the strings returned by a call to the Lua standard function `type()`.
Setters are much the same, formatted as `function __setindex_by_type(key, val)`.
