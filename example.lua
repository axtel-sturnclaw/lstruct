--[[
	A simple struct implementing a rudimentary immutable 2d vector.
	Only a small subset of operations are implemented here for the sake
	of conciseness.
--]]

local struct = require 'struct'
local vec2 = struct 'vec2'

-- Instantiation (vec = vec2()), (2vec = vec2(4, 5))
function vec2:__init(x, y)
	self.x, self.y = tonumber(x) or 0, tonumber(y) or 0
end

-- Addition (vec + 1), (vec + 2vec)
function vec2:__add(rhs)
	if type(rhs) == 'number' then
		return vec2(self.x + rhs, self.y + rhs)
	elseif vec2 <= rhs then -- (struct <= inst) checks inheritance
		return vec2(self.x + rhs.x, self.y + rhs.y)
	end
end

-- Subtraction (vec - 1), (vec - 2vec)
function vec2:__sub(rhs)
	if type(rhs) == 'number' then
		return vec2(self.x - rhs, self.y - rhs)
	elseif vec2 <= rhs then
		return vec2(self.x - rhs.x, self.y - rhs.y)
	end
end

-- Unary minus (-vec)
function vec2:__unm()
	return vec2(-self.x, -self.y)
end

-- Getter function (vec.mag)
function vec2:__get_mag()
	-- sqrt(x^2 + y^2) = |v|
	return math.sqrt(self.mag_s)
end
-- Getter function (vec.mag_s)
function vec2:__get_mag_s()
	-- The Pythagorean Theorem (a^2 + b^2 = c^2)
	-- is the basis for vector magnitude.
	return self.x^2 + self.y^2
end
-- #vec is equal to vec.mag
vec2.__len = vec2.__get_mag

-- Indexing a vector with a number (vec[n]) calls this function
function vec2:__index_by_number(num)
	if num == 1 then
		return self.x
	elseif num == 2 then
		return self.y
	end
end

-- Tostring function (print(vec))
function vec2:__tostring()
	return string.format('Vector 2D: %f %f', self.x, self.y)
end

-- A default zero value
vec2.zero = vec2(0, 0)

return vec2