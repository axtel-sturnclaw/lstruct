/*
 * file lstruct.h
 *
 * LStruct - Lightweight Lua Datatypes - C/C++ Interface
 * Copyleft 2016 Webster Sheets - All Wrongs Reserved.
 * See LICENSE for details.
 */

#include <lua.h>
#include <lauxlib.h>

#define LSTRUCT_TYPE "struct"
#define LSTRUCT_CONSTRUCTOR "__init"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Creates a new struct table at the top of the stack.
 *
 * If name is not NULL, adds the new struct to the registry at name,
 * and sets the key '__name' to the value of name in the new struct.
 *
 * If funcs is not NULL, it adds the contents of funcs to the struct.
 *
 * If name is not NULL and it already exists, it returns 0 and pushes
 * the value at name in the registry, ignoring the funcs pointer.
 */
int lstruct_newstruct(lua_State *L, const char *name, const luaL_Reg *funcs);

/*
 * If name is not NULL, attempts to get a struct at key name in the registry.
 * returns true if successful.
 */
int lstruct_getstruct(lua_State *L, const char *name);

/*
 * When passed a struct followed by a variable number of arguments,
 * it initializes an instance of the struct and calls it's constructor,
 * if it exists, with the passed arguments.
 *
 * The arguments are popped from the stack, and the new instance is
 * pushed onto the top of the stack.
 *
 */
int lstruct_instantiate(lua_State *L, int numargs);

/*
 * When passsed a struct instance followed by a number of arguments,
 * it calls the instance's constructor, if it exists.
 *
 * If the constructor function errors, the function returns the error code
 * from lua_pcall, and the error object is pushed on the stack.
 * If there is no error, it returns 0.
 *
 * If it is not passed a struct instance, returns -1;
 */
int lstruct_init(lua_State *L, int numargs);

/*
 * Checks if the object at idx is a struct base.
 */
int lstruct_isstruct(lua_State *L, int idx);

/*
 * Checks if the object at idx is a struct instance.
 */
int lstruct_isinstance(lua_State *L, int idx);

/*
 * Checks if a struct instance at idx inherits from the struct at the stack top.
 * returns false if either object is not a struct or there is no inheritance.
 */
int lstruct_inherits(lua_State *L, int idx);

/*
 * Like lstruct_checkinst, but returns 0 if false.
 */
int lstruct_testinst(lua_State *L, int idx, const char *name);

/*
 * Call `luaL_requiref(L, "lstruct", luaopen_lstruct, 0)` to load lstruct from C.
 */
extern int luaopen_lstruct(lua_State *L);

#ifdef __cplusplus
};
#endif
