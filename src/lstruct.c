/*
 * file lstruct.c
 *
 * LStruct - Lightweight Lua Datatypes - C/C++ Interface
 * Copyleft 2016 Webster Sheets - All Wrongs Reserved.
 * See LICENSE for details.
 */

#include "lstruct.h"

/* Core functions. */

static int __cont(lua_State *L, int status, lua_KContext ctx)
{
	(void) L;
	(void) status;

	return ctx;
}

/* Getter for structs */
static int __get_val(lua_State *L)
{
	lua_checkstack(L, 3);
	if (!lua_getmetatable(L, 1)) {
		/* No metatable, not a struct instance. */
		return 0;
	}

	/* Stack: struct, inst, key */
	lua_insert(L, 1);

	/* If the key is a string and not starting with '_'... */
	if (lua_type(L, 3) == LUA_TSTRING && *lua_tostring(L, 3) != '_') {
		/* Check for and run a getter */
		lua_pushstring(L, "__get_");
		lua_pushvalue(L, 3);
		lua_concat(L, 2);

		if (lua_gettable(L, 1) == LUA_TFUNCTION) {
			/* Stack: struct, func, inst */
			lua_insert(L, 2);
			lua_remove(L, 4);

			lua_callk(L, 1, 1, 1, __cont);
			return 1;
		}
		else {
			lua_pop(L, 1);
		}
	}

	/* Try __index_by_(type) */
	lua_pushstring(L, "__index_by_");
	lua_pushstring(L, lua_typename(L, lua_type(L, 3)));
	lua_concat(L, 2);

	if (lua_gettable(L, 1) == LUA_TFUNCTION) {
		/* Stack: struct, func, inst, key */
		lua_insert(L, 2);

		lua_callk(L, 2, 1, 1, __cont);
		return 1;
	}
	else {
		lua_pop(L, 1);
	}

	/* Just get the value. */
	lua_gettable(L, 1);

	return 1;
}

/* Setter for structs */
static int __set_val(lua_State *L)
{
	lua_checkstack(L, 4);
	if (!lua_getmetatable(L, 1)) {
		/* No metatable, not a struct instance. */
		return 0;
	}

	/* Stack: struct, inst, key, val */
	lua_insert(L, 1);

	/* If the key is a string and not starting with '_'... */
	if (lua_type(L, 3) == LUA_TSTRING && *lua_tostring(L, 3) != '_') {
		/* Check for and run a setter */
		lua_pushstring(L, "__set_");
		lua_pushvalue(L, 3);
		lua_concat(L, 2);

		if (lua_gettable(L, 1) == LUA_TFUNCTION) {
			/* Stack: struct, func, inst, val */
			lua_insert(L, 2);
			lua_remove(L, 4);

			lua_callk(L, 2, 1, 0, __cont);
			return 0;
		}
		else {
			lua_pop(L, 1);
		}
	}
	/* Try __setindex_by_(type) */
	lua_pushstring(L, "__setindex_by_");
	lua_pushstring(L, lua_typename(L, lua_type(L, 3)));
	lua_concat(L, 2);

	if (lua_gettable(L, 1) == LUA_TFUNCTION) {
		/* Stack: struct, func, inst, key, val */
		lua_insert(L, 2);

		lua_callk(L, 3, 1, 0, __cont);
		return 0;
	}
	else {
		lua_pop(L, 1);
	}

	/* Just set the value. */
	lua_rawset(L, 2);

	return 0;
}

/* Default __tostring implementation */
static int __tostring(lua_State *L)
{
	lua_settop(L, 1);
	lua_checkstack(L, 3);

	if (lstruct_isinstance(L, 1)) {
		lua_pushstring(L, "struct instance");
		lua_getmetatable(L, 1);
		lua_replace(L, 1);
	}
	else if (lstruct_isstruct(L, 1)) {
		lua_pushstring(L, "struct");
	}
	else {
		return luaL_error(L, "Attempt to call struct:__tostring on a non-struct value");
	}

	if (lua_getfield(L, 1, "__name") != LUA_TSTRING) {
		lua_remove(L, -1);
		return 1;
	}
	else {
		lua_pushstring(L, ": ");
		lua_insert(L, -2);
		lua_concat(L, 3);
	}

	return 1;
}

luaL_Reg struct_f[] = {
	{ "__index", __get_val },
	{ "__newindex", __set_val },
	{ "__tostring", __tostring },
	{ NULL, NULL }
};

/* C Interface */

int lstruct_newstruct(lua_State *L, const char *name, const luaL_Reg *funcs)
{
	lua_checkstack(L, 5);

	if (name == NULL) {
		/* With no name, we don't store the struct. */
		lua_newtable(L);
	}
	else {
		if (!luaL_newmetatable(L, name)) {
			/* This name exists in the registry, so we assume
			the struct has already been initialized. */
			return 0;
		}
		else {
			lua_pushstring(L, name);
			/* newmetatable sets the _name field; we need the __name field too. */
			lua_setfield(L, -2, "__name");
		}
	}

	/* Set the __type field. */
	lua_pushstring(L, LSTRUCT_TYPE);
	lua_setfield(L, -2, "__type");

	if (funcs != NULL) {
		luaL_setfuncs(L, funcs, 0);
	}

	/* Set the other fields. */
	luaL_setfuncs(L, struct_f, 0);
	/* Set the metatable */
	luaL_setmetatable(L, "lstruct.structmt");

	return 1;
}

int lstruct_getstruct(lua_State *L, const char *name)
{
	if (name == NULL) return 0;
	lua_checkstack(L, 1);

	if ((lua_getfield(L, LUA_REGISTRYINDEX, name) == LUA_TTABLE) && lstruct_isstruct(L, -1)) {
		return 1;
	}

	lua_pop(L, 1);

	return 0;
}

/* Instantiate a struct. */
int lstruct_instantiate(lua_State *L, int numargs)
{
	/* Points to the index of the struct */
	int sidx = lua_absindex(L, -(numargs + 1));
	if (!lstruct_isstruct(L, sidx)) return -1;

	lua_checkstack(L, 1);
	lua_newtable(L);

	/* Move the struct to the top of the stack */
	lua_rotate(L, sidx, -1);
	/* Set the metatable */
	lua_setmetatable(L, -2);
	/* Move the instance below the args */
	lua_insert(L, sidx);

	return lstruct_init(L, numargs);
}

/* Run the constructor */
int lstruct_init(lua_State *L, int numargs)
{
	/* The index of the struct instance */
	int idx = lua_absindex(L, -(numargs + 1));
	if (!lstruct_isinstance(L, idx)) return -1;

	/* get the constructor from the instance */
	if (lua_getfield(L, idx, LSTRUCT_CONSTRUCTOR) == LUA_TFUNCTION) {
		/* Move the function and a copy of the instance before the arguments */
		lua_pushvalue(L, idx);
		lua_rotate(L, idx+1, 2);

		/* Call the constructor. */
		return lua_pcall(L, numargs+1, 0, 0);
	}
	else {
		/* Remove the non-function value and the args */
		lua_pop(L, numargs + 1);
		return LUA_OK;
	}
}

/* Check if the object at idx is a struct base */
int lstruct_isstruct(lua_State *L, int idx) {
	lua_checkstack(L, 2);
	if (!lua_getmetatable(L, idx)) {
		/* Object has no metatable, not a struct. */
		return 0;
	}
	luaL_getmetatable(L, "lstruct.structmt");

	/* If the object's metatable is the base metatable
	for structs, it's a struct */
	int res = lua_rawequal(L, -1, -2);

	lua_pop(L, 2);
	return res;

}

/* Check if the object at idx is a struct instance */
int lstruct_isinstance(lua_State *L, int idx) {
	lua_checkstack(L, 1);
	if (!lua_getmetatable(L, idx)) {
		/* No metatable, not a struct instance */
		return 0;
	}

	/* If the object's metatable is a struct, the object is a struct instance */
	int res = lstruct_isstruct(L, -1);

	lua_pop(L, 1);
	return res;
}

/*
Check if the object at idx is a struct instance inheriting from the struct
at the top of the stack.
*/
int lstruct_inherits(lua_State *L, int idx) {
	lua_checkstack(L, 1);
	if(!lstruct_isstruct(L, -1) || !lua_getmetatable(L, idx)) {
		/* Not a struct at the stack top, or the instance has no metatable,
		therefore it is not an instance. */
		return 0;
	}

	/* Check if the instance's metatable is the struct at the top of the stack. */
	int res = lua_rawequal(L, -1, -2);

	lua_pop(L, 1);
	return res;
}

/*
test if the value at idx is a struct instance inheriting from the struct
registered with name
*/
int lstruct_testinst(lua_State *L, int idx, const char *name)
{
	int res = 0;
	if (lstruct_getstruct(L, name)){
		if (lstruct_inherits(L, idx)) {
			res = 1;
		}

		lua_pop(L, 1);
	}

	return res;
}

/* Lua Interface */

/* Wrapper for struct creation. */
static int __new_struct(lua_State *L)
{
	const char *name = luaL_optstring(L, 1, NULL);
	lua_settop(L, 0);

	int existing = !lstruct_newstruct(L, name, NULL);

	lua_pushboolean(L, existing);

	return 2;
}

/* Wrapper for struct instantiation */
static int __inst_struct(lua_State *L)
{
	if (!lstruct_isstruct(L, 1)) {
		return luaL_error(L, "Attempt to instantiate a non-struct object\n");
	}

	int ok = lstruct_instantiate(L, lua_gettop(L) - 1);
	if (ok != LUA_OK) {
		return lua_error(L);
	}

	return 1;
}

/* A <= b = true when b is an instance of struct A */
static int __check_inheritance(lua_State *L)
{
	lua_settop(L, 2);
	lua_insert(L, 1);
	lua_pushboolean(L, lstruct_inherits(L, 1));

	return 1;
}

luaL_Reg smt_f[] = {
	{ "__call", __inst_struct },
	{ "__tostring", __tostring },
	{ "__le", __check_inheritance },
	{ NULL, NULL }
};

/* Load the module */
extern int luaopen_lstruct(lua_State *L) {
	if (lua_getfield(L, LUA_REGISTRYINDEX, "lstruct") == LUA_TFUNCTION) {
		/* Already registered. */
		return 1;
	}
	else {
		/* remove the nil value */
		lua_pop(L, 1);
	}

	/* Set up the metatable for structs. */
	luaL_newmetatable(L, "lstruct.structmt");
	luaL_setfuncs(L, smt_f, 0);
	lua_pushstring(L, LSTRUCT_TYPE);
	lua_setfield(L, -2, "__metatable");
	lua_pop(L, 1);

	/* Push the struct() function */
	lua_pushcfunction(L, __new_struct);

	/* Install it in the registry table */
	lua_pushvalue(L, -1);
	lua_setfield(L, LUA_REGISTRYINDEX, "lstruct");

	return 1;

}
