--[[
    file lstruct.lua / init.lua

    LStruct - Lightweight Lua Datatypes - Lua Interface
    Copyleft 2016 Webster Sheets - All Wrongs Reserved.
    See LICENSE for details.
--]]

local smt

local function __get(sinst, key)
  local struct, getter = getmetatable(sinst), nil
  if type(key) == 'string' and key:sub(1,1) ~= '_' then
    getter = struct["__get_"..key]
    if type(getter) == 'function' then
      return getter(sinst)
    end
  end
  getter = struct["__index_by_"..type(key)]
  if type(getter) == 'function' then
    return getter(sinst, key)
  end
  return struct[key]
end

local function __set(sinst, key, val)
  local struct, setter = getmetatable(sinst), nil
  if type(key) == 'string' and key:sub(1,1) ~= '_' then
    setter = struct["__set_"..key]
    if type(setter) == 'function' then
      return setter(sinst, val)
    end
  end
  setter = struct["__setindex_by_"..type(key)]
  if type(setter) == 'function' then
    return setter(sinst, key, val)
  end
  rawset(sinst, key, val)
end

local function __inst(struct, ...)
  local new = setmetatable({}, struct)
  if type(struct.__init) == 'function' then
    struct.__init(new, ...)
  end
  return new
end

local function __tostring(struct)
  local parent, type = getmetatable(struct), 'struct'
  if parent ~= smt then struct = parent; type = 'struct instance' end
  return type .. (struct.__name and ": " .. struct.__name or "")
end

local function __checkinst(self, other)
	return getmetatable(other) == self
end

smt = { __call = __inst, __tostring = __tostring, __le = __checkinst}

local function struct(name)
  local struct = setmetatable({__name = name, _name = name, __type = 'struct'}, smt)
  struct.__index = __get
  struct.__newindex = __set
  struct.__tostring = __tostring
  return struct
end

return struct
